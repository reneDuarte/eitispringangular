/**
 * criar um modulo CadastroTarefa, injetando uma depedencia ngMaterial
 */

/**
 * scrol em table
 */
angular.module('CadastroUsuario', ['ngMaterial']);

/**
 * criar um controller no modulo CadastroUsuario passando como depedencia $scope, $http, $timeout
 * $scope: O escopo � a parte de liga��o entre o HTML (view) e o JavaScript (controller).
 * $http:  Faz uma solicita��o ao servidor e retorna uma resposta. Possue alguns metodos que utilizei algumas no projeto
 *          .delete()
 *          .get()
 *          .head()
 *          .jsonp()
 *          .patch()
 *          .post()
 *          .put()
 *
 * $timeout: Atraso em milissegundos na excecu��o de um metodo, utilizado no metodo mostrarMSG()
 *          ex:
 *          $timeout(function () {
     *          delete $scope.message;
 *          }, 2000)
 *
 *
 */
angular.module('CadastroUsuario').controller('CadastroUsuarioCtrl', function ($scope, $http, $timeout) {
    $scope.app = "CadastroUsuario";
    $scope.listaUsuario = [];
    $scope.usuario = {};

    $scope.listar  = function () {

        $http.post('/rest/comando/listar').then(
            function (request) {
                $scope.listaUsuario = request.data.customerList;
                if(!$scope.listaUsuario){
                    $scope.listaUsuario = [];
                }
            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro);
            }
        );

    };


    /**
     * Busca Persistente por id
     *
     * @param usuario.id
     */
    $scope.getForId = function (usuario) {

        $http.get('/rest/comando/buscar/'+ NOME_CLASSE +'@' + usuario.id).then(
            function (request) {
                /**
                 * recupera bean do retorno;
                 */
                $scope.usuario = request.data.bean;

            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro)

            }
        )
    };

    $scope.cancelar = function () {
        if ($scope.usuario) {
            delete $scope.usuario;
        }
        $scope.usuario = {};
    };


    $scope.mostrarError = function (ele) {

        var mask = ele.$$attr.uiMask;
        if (ele.$invalid) {
            var campo = ele.$$attr.placeholder;
            var mask = ele.$$attr.uiMask;

            var msg = 'Erro para validar o campo;' + campo;
            if (mask)
                msg += "\nFavor digitar o valor no formato " + mask;

            mostrarMSG(msg);
            ele.$$element[0].focus();
            return ele.$invalid;
        }

    };

    $scope.deletar = function (usuario) {
        if (confirm('Deseja deletar o usuário ' + usuario.name)) {
            $http.delete('/rest/comando/delete/'+usuario.id).then(
                function (request) {
                    if (request.data.sucesso) {

                        $scope.listaUsuario = $scope.listaUsuario.filter(function (obj) {
                            if (usuario.id != obj.id) return obj;
                        });

                        if ($scope.usuario)
                            $scope.cancelar();

                    } else {
                        /**
                         * Mostra erro
                         * @see mostrarMSG
                         */
                        mostrarMSG("Aconteceu um problema: " + request.data.erro);
                    }
                },
                function (request) {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro);

                }
            )
        }

    };

    $scope.editar = function (usuario) {

        $http.put('/rest/comando/editar', usuario).then(
            function (request) {

                usuario = request.data.bean;
                if (request.data.sucesso) {

                    $scope.listaUsuario.some(function (el, i) {
                        if (el.id == usuario.id) {
                            $scope.listaUsuario.splice(i, 1, angular.copy(usuario));
                            return true;
                        }
                    });

                    $scope.cancelar();

                } else {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro);

                    $scope.cancelar();
                }
            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro);
                $scope.cancelar();
            }
        )

    };

    $scope.salvar = function (usuario, naoValidarCampos) {
        var erro = false;
        /**
         * Imnpede a verifica��o de erro de preenchimento de campo
         */
        if (!naoValidarCampos) {

            this.frmUsuario.$$controls.forEach( function(ele){
                if (!erro)
                    erro = $scope.mostrarError(ele);
            } )


        }

        if (!erro) {
            if (!usuario.id)
                $scope.adicionar(usuario);
            else
                $scope.editar(usuario);
        }
    };

    $scope.trocarStatus = function (usuario) {
        usuario.concluido = !usuario.concluido;
        $scope.salvar(usuario, true);
        $scope.listar();
    };

    $scope.novaLista = function(){
        window.location.reload();
    }

    $scope.adicionar = function (usuario) {

        $http.post('/rest/comando/salvar', usuario).then(
            function (request) {

                if (request.data.sucesso) {
                    usuario = request.data.bean;

                    $scope.listaUsuario.push(angular.copy(usuario));
                    $scope.cancelar();

                } else {
                    /**
                     * Mostra erro
                     * @see mostrarMSG
                     */
                    mostrarMSG("Aconteceu um problema: " + request.data.erro)
                }

            },
            function (request) {
                /**
                 * Mostra erro
                 * @see mostrarMSG
                 */
                mostrarMSG("Aconteceu um problema: " + request.data.erro)

            }
        )


    };

    function mostrarMSG(msg) {

        $scope.message = msg;

        $timeout(function () {
            delete $scope.message;
        }, 2000)
    }


    $scope.ordenarPor = function (campo) {

        $scope.criterio = campo;
        $scope.ordem = !$scope.ordem;

    };



});

criarUiMask('CadastroUsuario');
