package br.com.eiti.service;


import br.com.eiti.bean.Users;
import br.com.eiti.dao.Dao;
import br.com.eiti.exceptions.ExceptionsCreateEntityManager;
import br.com.eiti.util.MD5;

import javax.persistence.Query;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;


public class ServiceUser {

    public Users salvar(Users users) throws Exception {
        Dao dao = new Dao();
        tratarData(users);
        tratarSenha(users);
        dao.salvar(users);
        return users;
    }

    private void tratarSenha(Users users) {
        if(users.getPassword() != null && !"".equals(users.getPassword())){
            try {
                users.setPassword(MD5.encode(users.getPassword()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    private void tratarData(Users users) {
        if(users.getRegister_date() == null ){
            users.setRegister_date(new Date());
        }
    }


    public void deletar(Users objAtiv) throws Exception {
        Dao dao = new Dao();
        objAtiv = (Users) dao.buscarId(objAtiv.getClass().getName(), objAtiv.getId());
        dao.deletar(objAtiv);


    }

    public List<Users> listarUsers() throws ExceptionsCreateEntityManager {
        Dao dao = null;
        try {
            dao  = new Dao();
            Query query = dao.getSession().createQuery("from Users");
            return  query.getResultList();
        }catch (Exception e){
           e.printStackTrace();
        }finally {
            if (dao != null)
                dao.fecharsession();
        }

        return  null;
    }
}
