package br.com.eiti.rest;


import br.com.eiti.bean.Bean;

import javax.persistence.Entity;
import java.util.List;


/**
 * @author rene.duarte
 * <p>
 * Classe de retorno contendo as seguintes propriedades:
 * @see #customerList
 * @see #erro
 * @see #sucesso
 * @see #bean
 */
@Entity
public class Retorno {

    /**
     * aceita só objeto que extend Bean
     */
    private List<? extends Bean> customerList;
    private String erro;
    private boolean sucesso = false;
    private Bean bean;

    /**
     * @return List
     */
    public List<? extends Bean> getCustomerList() {
        return customerList;
    }

    /**
     * @param customerList
     */
    public void setCustomerList(List<? extends Bean> customerList) {
        this.customerList = customerList;
    }

    /**
     * @return erro
     */

    public String getErro() {
        return erro;
    }

    /**
     * @param message
     */
    public void setErro(String message) {

        this.erro = message;
    }

    /**
     * @return boolean
     */
    public boolean isSucesso() {
        return sucesso;
    }

    /**
     * @param sucesso
     */
    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    /**
     * @param bean
     */
    public void setCustomer(Bean bean) {

        this.bean = bean;
    }

    /**
     * @return Bean
     */
    public Bean getBean() {

        return bean;
    }






}
