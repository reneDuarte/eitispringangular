package br.com.eiti.dao;



import br.com.eiti.bean.Bean;
import br.com.eiti.exceptions.ExceptionsCreateEntityManager;
import org.hibernate.Session;

import java.util.List;

public interface Idao {

    public Bean salvar(final Bean bean) throws Exception;
    public Boolean deletar(final Bean bean) throws ClassNotFoundException, ExceptionsCreateEntityManager;
    public List listar(final Class<? extends Bean> zClass);
    public Session getSession() throws ExceptionsCreateEntityManager;
    public Bean buscarId(final String nomeClasse, final Integer id) throws ClassNotFoundException, Exception;

}
